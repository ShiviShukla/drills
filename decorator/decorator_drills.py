import math
import time
import functools

def timer(func):
    def wrapper_get_time(x):
        starttime = time.perf_counter()
        time.sleep(1)
        print(f"executing function '{func.__name__}' and retutn value is >> ",func(x))
        endtime = time.perf_counter()

        exec_time = endtime - starttime
        return exec_time
    return wrapper_get_time


def generic_timer(func):
    def wrapper_get_time(*args, **kwargs):
        starttime = time.perf_counter()
        time.sleep(1)
        print(f"executing function '{func.__name__}' and retutn value is >> ",func(*args, **kwargs))
        endtime = time.perf_counter()

        exec_time = endtime - starttime
        return exec_time
    return wrapper_get_time

def memoize(f):
    memo = {}
    def wrapper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return wrapper

@timer
def square(x):
    return x*x

@timer
def sqrt(x):
    return math.sqrt(x)

@generic_timer
def add(x, y):
    return x + y

@generic_timer
def add3(x, y, z):
    return x + y + z

@generic_timer
def add_any(*args):
    return sum(args)

@generic_timer
def abs_add_any(*args, **kwargs):

    total = sum(args)
    if kwargs.get('abs') is True:

        return abs(total)
    return total


@memoize
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)

@functools.lru_cache(maxsize=1000)
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    print(fact)
    return fact




if __name__ == '__main__':
    # print(square(4))
    # print(sqrt(4))
    # add(2,3)
    # add3(10,20,30)
    # add_any(1,2,5,3,8,7,9)
    # abs_add_any(1,4,5,8,-200.58, abs=1)
    # fib(5)
    # factorial(5)
    pass
